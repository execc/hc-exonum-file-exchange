declare module 'exonum-client' {
    const Exonum: any;
    export = Exonum;
}

declare module 'ipfs' {
    const IPFS: any;
    export = IPFS;
}