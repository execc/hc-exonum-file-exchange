import Exonum from 'exonum-client';
/*
let sendFunds = Exonum.newMessage({
    protocol_version: 0,
    service_id: 130,
    message_id: 128,
    fields: [
      { name: 'from', type: Exonum.Hash },
      { name: 'to', type: Exonum.Hash },
      { name: 'amount', type: Exonum.Uint64 }
    ]
  })

  // Signing key pair
const keyPair = {
    publicKey: 'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a',
    secretKey: '978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b' +
    'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a'
  }
  
  // Transaction data to be signed
  const data = {
    from: 'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a',
    to: 'f7ea8fd02cb41cc2cd45fd5adc89ca1bf605b2e31f796a3417ddbcd4a3634647',
    amount: 1000
  }
  
  // Sign the data
  let signature = sendFunds.sign(keyPair.secretKey, data)

  // Define transaction handler address
const transactionEndpoint = 'http://127.0.0.1:8200/api/services/cryptocurrency/v1/wallets'

// Define transaction explorer address
const explorerBasePath = 'http://127.0.0.1:8200/api/explorer/v1/transactions?hash='

sendFunds.send(transactionEndpoint, explorerBasePath, data, signature).then((txHash: any) => {})
*/

export const newMessage = (service_id: string, message_id: string, fields: any[]) => ({
    type: 'EXONUM_NEW_MESSAGE',
    payload: {
        protocol_version: 0,
        service_id,
        message_id,
        fields
    }
});

export const setKeyPair = (secretKey: string) => (console.log(secretKey), {
    type: 'EXONUM_SET_KEYPAIR',
    payload: {
        publicKey: secretKey.slice(64, 128),
        secretKey: secretKey
    }
});

export const setMessageData = (from: string, to: string) => ({
    type: 'EXONUM_SET_MESSAGE_DATA',
    payload: {
        from,
        to,

    }
})