import CryptoJS from 'crypto-js';
import sha256 from 'crypto-js/sha256';
import JSEncript from 'jsencrypt';
import { contacts } from './containers/Send/addressbook';

export const getUuid4 = () => {
    // @ts-ignore
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}

export const encrypt = (text: string, publicKey: string) => {
    var jsencrypt = new JSEncript();
    jsencrypt.setPublicKey(publicKey);
    return jsencrypt.encrypt(text);
}

export const decrypt = (text: string, secretKey: string) => {
    var jsencrypt = new JSEncript();
    jsencrypt.setPrivateKey(secretKey);
    return jsencrypt.decrypt(text);
}

export const getStringHash = string => sha256(string).toString();

export const getFileContent = (file) => {
    // @ts-ignore
    return new Promise((resolve, reject) => {
      const reader = new FileReader;
      reader.onload = function() {
        try {
          // @ts-ignore
          resolve(reader.result);
        } catch (error) {
          reject(error)
        }
      }

      reader.readAsBinaryString(file);
    })
  };

export const getFileHash = file => {
    // @ts-ignore
    return new Promise((resolve, reject) => {
      const reader = new FileReader;
      reader.onload = function() {
        try {
          const hash = CryptoJS.algo.SHA256.create()
          // @ts-ignore
          hash.update(CryptoJS.enc.Latin1.parse(reader.result))
          resolve('' + hash.finalize())
        } catch (error) {
          reject(error)
        }
      }

      reader.readAsBinaryString(file);
    })
  };

export const findPersonByPublicKey = publicKey => {
    const person = contacts.filter(contact => contact.bcPublicKey === publicKey);
    return person ? `${person[0].surname} ${person[0].name}` : null;
}