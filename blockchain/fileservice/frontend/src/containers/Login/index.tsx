import * as React from 'react';
import { Divider, Input, Upload, Icon, Row, Col, Button } from 'antd';
import { UploadChangeParam } from 'antd/lib/upload/interface';
import { connect } from 'react-redux';
import { setKeyPair } from '../../actions/exonum'
import Register from './Register';
import * as Exonum from 'exonum-client';

const mapStateToProps = (state: any) => ({});
const mapDispatchToProps = (dispatch: any) => ({
    login: (key: string) => dispatch(setKeyPair(key))
});

export class Login extends React.Component<any, any> {

    state = {
        textInput: '',
        register: false,
        key: {}
    }

    readFile = (event: any) => {
        this.props.login(event.target.result.slice(0, 128));
    }

    handleUploadOnChange = (info: UploadChangeParam) => {
        const file: any = info.file;
        console.log(file);
        const fr = new FileReader();

        fr.addEventListener('load', this.readFile);
        fr.readAsText(file);
    }

    register = () => {
        // @ts-ignore
        this.setState({register: true, key: Exonum.keyPair().secretKey});
    }

    componentWillUnmount () {

    }

    render() {
        return (
            this.state.register ? <Register newKey={this.state.key} /> : <div>
                <h1 style={{padding: '0 5%', marginTop: 15}}>Авторизация</h1>
                <Row>
                    <Col span={4} />
                        Нет ключа? <a href="javascript:;" onClick={this.register}>Зарегистрироваться</a>
                    <Col span={1} />
                </Row>
                <Divider orientation="left">По секретному ключу</Divider>
                <Row>
                    <Col span={4} />
                    <Col span={15}>
                        <Input value={this.state.textInput} onChange={e => this.setState({textInput: e.target.value})} />
                    </Col>
                    <Col span={2}>
                        <Button type="primary" style={{marginLeft: 15, background: '#33AA22'}} onClick={() => this.props.login(this.state.textInput)}>
                            Войти
                        </Button>
                    </Col>
                    <Col span={1} />
                </Row>

                <Divider orientation="left">По ключ-файлу</Divider>
                <Row>
                    <Col span={4} />
                    <Col span={17}>
                        <Upload.Dragger name="file" onChange={this.handleUploadOnChange} beforeUpload={() => false} showUploadList={false}>
                            <p className="ant-upload-drag-icon">
                                <Icon type="inbox" />
                            </p>
                            <p className="ant-upload-text">Кликните или перетащите ключ-файл в эту область для авторизации</p>
                        </Upload.Dragger>
                     </Col>
                     <Col span={1} />
                </Row>
           
        </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);