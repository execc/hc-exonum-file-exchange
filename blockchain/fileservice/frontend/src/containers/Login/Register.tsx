import * as React from 'react';
import { Divider, Input, Upload, Icon, Row, Col, Button } from 'antd';
import { UploadChangeParam } from 'antd/lib/upload/interface';
import { connect } from 'react-redux';
import { setKeyPair } from '../../actions/exonum'

const mapStateToProps = (state: any) => ({});
const mapDispatchToProps = (dispatch: any) => ({
    login: (key: string) => dispatch(setKeyPair(key))
});

export default class Register extends React.Component<any, any> {
    render() {
        return (
            <div>
                <h1 style={{padding: '0 5%', marginTop: 15}}>Регистрация</h1>
                <Divider orientation="left">Новый ключ</Divider>
                <Row>
                    <Col span={4} />
                    <Col span={15}>
                        {this.props.newKey}
                    </Col>
                    <Col span={2}>
                        <Button type="primary" style={{marginLeft: 15}}
                            download="chain.key"
                            href={`data:text/plain;charset=utf-8,${encodeURIComponent(this.props.newKey)}`}
                        >
                            Скачать
                        </Button>
                    </Col>
                    <Col span={1} />
                </Row>           
            </div>
        );
    }
}
