import * as Exonum from 'exonum-client';

const transactionEndpoint = '/api/services/fileservice/v1/files';
const explorerBasePath = '/api/explorer/v1/transactions?hash=';

let FileData = Exonum.newType({
    fields: [
      { name: 'file_hash', type: Exonum.Hash },
      { name: 'exchange_uuid', type: Exonum.Hash },
      { name: 'ipfs_hash', type: Exonum.String },
      { name: 'description', type: Exonum.String },
      { name: 'file_name', type: Exonum.String },
      { name: 'file_type', type: Exonum.String }
    ]
});

export const File = Exonum.newType({
    fields: [
        { name: 'sender', type: Exonum.PublicKey },
        { name: 'receiver', type: Exonum.PublicKey },
        { name: 'data', type: FileData }
    ]
});

export const sendFile = (secretKey: string, data: any) => {
    let uploadFile = Exonum.newMessage({
        protocol_version: 0,
        service_id: 130,
        message_id: 0,
        fields: [
            { name: 'sender', type: Exonum.PublicKey },
            { name: 'receiver', type: Exonum.PublicKey },
            { name: 'content', type: FileData }
        ]
    });
    
    let signature = uploadFile.sign(secretKey, data);

    return uploadFile.send(transactionEndpoint, explorerBasePath, data, signature, 30).then((res: any) => {
        console.log(res);
    }).then((err: any) => {
        console.error(err);
    })
}
