export interface UploadFileFormat {
    sender: string;
    receiver: string;
    content: {
        file_hash?: string;
        exchange_uuid: string;
        ipfs_hash: string;
        description: string;
        file_name: string;
        file_type: string;
    }
}