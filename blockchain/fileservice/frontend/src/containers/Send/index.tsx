import * as React from 'react';
import { Upload, Icon, Row, Col, Select } from 'antd';
import { UploadChangeParam } from 'antd/lib/upload/interface';
import { UploadFileFormat } from './interfaces';
import { connect } from 'react-redux';
import { sendFile } from './sendTransaction';
import { getStringHash, getFileHash, getUuid4, getFileContent } from '../../utils';
import { ipfs } from '../..';
import { contacts } from './addressbook';

const mapStateToProps = (state: any) => ({
    publicKey: state.exonum.keys.publicKey,
    secretKey: state.exonum.keys.secretKey
});

/*
* chain.key = Кирилл Михайлов
* chain (1).key = Денис Орлов
*/
export class Send extends React.Component<any, any> {

    state = {
        receiverRSA: '',
        receiverBC: '',
        description: ''
    }

    handleUploadOnChange = (info: UploadChangeParam) => {
        const file = info.file;

        getFileContent(file).
            then(content => {
                ipfs.files.add(ipfs.Buffer.from(content), (err, res) => {
                    const uploadFile: UploadFileFormat = {
                        sender: this.props.publicKey,
                        receiver: this.state.receiverBC,
                        content: {
                            description: this.state.description,
                            exchange_uuid: getStringHash(getUuid4()),
                            ipfs_hash: res[0].hash,
                            file_hash: undefined,
                            file_name: file.name,
                            file_type: file.type
                        }
                    }

                    getFileHash(file).then(hash => {
                        uploadFile.content.file_hash = hash;
                        sendFile(this.props.secretKey, uploadFile);
                    });
                })
            });
    }

    handleSelectPerson: any = value => {
        const [bc, rsa] = value.split('|||||');
        this.setState({
            receiverRSA: rsa,
            receiverBC: bc
        })
    }

    render() {
        return (
            <div>
                <h1>Отправить файл</h1>
                <Row>
                    <Col span={4}>Получатель:</Col>
                    <Col span={8}>
                        <Select showSearch optionFilterProp="children" style={{ width: '100%' }} onChange={this.handleSelectPerson}
                            filterOption={(input, option: any) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            >
                            {contacts.map((item, index) => (
                                <Select.Option value={`${item.bcPublicKey}|||||${item.rsaPublicKey}`} key={index.toString()}>
                                    {item.surname + ' ' + item.name}
                                </Select.Option>
                            ))}
                        </Select>
                    </Col>
                </Row>
                <Upload.Dragger name="file" onChange={this.handleUploadOnChange} beforeUpload={() => false} showUploadList={false}>
                    <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                    </p>
                    <p className="ant-upload-text">Кликните или перетащите файл в эту область для загрузки</p>
                    <p className="ant-upload-hint">Поддерживается загрузка только одного файла</p>
                </Upload.Dragger>
            </div>);
    }
}

export default connect(mapStateToProps)(Send);