import * as React from 'react';
import { Divider, Table } from 'antd';
import { connect } from 'react-redux';
const { Column, ColumnGroup } = Table;
import { ipfs } from '../../'

const mapStateToProps = (state: any) => ({
    publicKey: state.exonum.keys.publicKey,
    inboxItems: state.inbox.items
})

const normalizeData = (data: any[]) => {
    const output = data.map(row => {
        for (var i in row.data) {
            if (row.data.hasOwnProperty(i)) {
                row['data_' + i] = row.data[i];
            }
        }

        for (var i in row.time) {
            if (row.time.hasOwnProperty(i)) {
                row['time_' + i] = row.time[i];
            }
        }

        delete row.data;
        delete row.time;
        return row;
    });
    console.log(output);
    return output;
}

export class Inbox extends React.Component<any, any> {

    state = {
        items: []
    }

    componentDidMount() {
        fetch('/api/services/fileservice/v1/files/receiver?key=' + this.props.publicKey)
            .then(res => res.json())
            .then(items => this.setState({items: normalizeData(items)}))
    }

    download = (ipfs_hash, row) => () => {
        ipfs.files.get(row.data_ipfs_hash, (err, res) => {
            var link = document.createElement('a')
            link.download = row.data_file_name;
            link.href=`data:${row.data_file_type};charset=utf-8,${encodeURIComponent(res[0].content.toString('utf-8'))}`
            link.style.display = 'none';
            document.body.appendChild(link);
            link.click();
        });
    }

    render() {
        return (
            <div className="inbox-page">
                <h1>Входящие файлы</h1>
                <Divider />
                <Table dataSource={this.state.items}>
                    <Column title="Имя файла" dataIndex="data_file_name" key="filename" />
                    <Column title="Время отправки" dataIndex="time_secs" key="senttime" render={
                        time_secs => new Date(time_secs * 1000).toString()
                    }/>
                    <Column title="" key="data_ipfs_hash" dataIndex="" render={
                        (ipfs_hash, row) => <a href="javascript:;" onClick={this.download(ipfs_hash, row)}>Скачать</a>
                    } />
                </Table>
            </div>
        );
    }
}

export default connect(mapStateToProps)(Inbox);