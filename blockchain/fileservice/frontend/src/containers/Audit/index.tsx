import * as React from 'react';
import { connect } from 'react-redux'
import { Divider, Row, Col, Input, Button } from 'antd';
import * as Exonum from 'exonum-client';
import { File } from '../Send/sendTransaction';
import { contacts } from '../Send/addressbook';

const mapStateToProps = (state: any) => ({});

export class Audit extends React.Component<any, any> {

    state = {
        fileHash: '',
        file: null
    }

    findPersonByPublicKey = publicKey => {
        const person = contacts.filter(contact => contact.bcPublicKey === publicKey);
        return person ? person[0] : null;
    }

    onFind = () => {
        fetch('/api/services/fileservice/v1/files/proof?hash=' + this.state.fileHash)
            .then(res => res.json())
            .then(proof => {
                this.setState({ file: proof });
                proof = new Exonum.MapProof(proof.file_proof, Exonum.PublicKey, File);
                console.log(proof);
            })
    }

    render () {
        let file;

        if (this.state.file != null) {
            // @ts-ignore
            if (this.state.file.file_proof.entries.length &&
                // @ts-ignore
            this.state.file.file_proof.entries[0]) {
                // @ts-ignore
                file = this.state.file.file_proof.entries[0].value;
            }
        }

        return (
            <div>
                <h1>Аудит</h1>
                <Divider />
                <Row>
                    <Col xs={1} />
                    <Col xs={3}>Хэш файла:</Col>
                    <Col xs={16}>
                        <Input onChange={e => this.setState({fileHash: e.target.value})} value={this.state.fileHash}/>
                    </Col>
                    <Col xs={3}>
                        <Button type="primary" onClick={this.onFind}>Найти</Button>
                    </Col>
                    <Col xs={1} />
                </Row>
                <Divider />
                {this.state.file !== null ? (
                    <div>
                        <Row>
                            <Col xs={1} />
                            <Col xs={3}>Отправитель:</Col>
                            <Col xs={19}>
                                {this.findPersonByPublicKey(file.sender)}
                            </Col>
                            <Col xs={1} />
                        </Row>

                        <Row>
                            <Col xs={1} />
                            <Col xs={3}>Получатель:</Col>
                            <Col xs={19}>
                                {this.findPersonByPublicKey(file.receiver)}
                            </Col>
                            <Col xs={1} />
                        </Row>

                        <Row>
                            <Col xs={1} />
                            <Col xs={3}>Дата и время отправки:</Col>
                            <Col xs={19}>
                                {new Date(file.time.secs * 1000).toString()}
                            </Col>
                            <Col xs={1} />
                        </Row>
                    </div>
                ) : null}
            </div>
        )
    }
}

export default connect(mapStateToProps)(Audit)