export { Inbox as InboxPlain, default as Inbox } from './Inbox';
export { default as Send } from './Send';

export { Login as LoginPlain, default as Login } from './Login';
export { Audit as AuditPlain, default as Audit } from './Audit';