import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Layout, Menu, Icon, Badge, Avatar, Row, Col } from 'antd';
import { ConnectedRouter, push } from 'connected-react-router';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router';
const { Sider, Content, Footer, Header } = Layout;
import { history } from './';
import { Send, Inbox, Login, Audit } from './containers';
import { findPersonByPublicKey } from './utils';

const mapStateToProps = (state: any) => ({
  publicKey: state.exonum.keys.publicKey,
  secretKey: state.exonum.keys.secretKey
});

const mapDispatchToProps = (dispatch: any) => ({
  goToInbox: () => dispatch(push('/inbox')),
  goToSend: () => dispatch(push('/send')),
  goToAudit: () => dispatch(push('/audit'))
});

class App extends Component<any, any> {
  render() {
    return (
      <ConnectedRouter history={history}>
        <Layout style={{ minHeight: '100vh' }}>
          {this.props.secretKey ? (<Sider
            collapsible
          >
            <Menu>
              <Menu.Item onClick={this.props.goToInbox}>
                <Icon type="inbox" />
                <span>Входящие</span>
              </Menu.Item>
              <Menu.Item onClick={this.props.goToSend}>
                <Icon type="upload" />
                <span>Отправить</span>
              </Menu.Item>
              <Menu.Item onClick={this.props.goToAudit}>
                <Icon type="audit" />
                <span>Аудит</span>
              </Menu.Item>
            </Menu>
          </Sider>) : null}
          <Layout>
            <Header style={{color: '#fff', background: '#33AA22'}}>
              {this.props.secretKey ? (<Row>
                <Col style={{float: 'right', textAlign: 'right'}} span={12}>
                      {findPersonByPublicKey(this.props.publicKey)} <Avatar icon="user" />
                </Col>
              </Row> ) : null}
            </Header>
            <Content style={{padding: '15px 25px'}}>
              {this.props.secretKey ? (
              <Switch>
                <Route path="/send" component={Send} />
                <Route path="/inbox" component={Inbox} />
                <Route path="/login" component={Login} />
                <Route path="/audit" component={Audit} />
                <Route path="/" />
              </Switch>
              ) : <Login />}
            </Content>
            <Footer>

            </Footer>
          </Layout>
        </Layout>
      </ConnectedRouter>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
