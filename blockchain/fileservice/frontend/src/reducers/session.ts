import { sessionMock } from './session-mock';

const INITIAL_SESSION_STATE = {
    keys: {
        privateKey: null,
        publicKey: null
    }
}

export const session = (state = INITIAL_SESSION_STATE, action: any) => {
    switch (action.type) {
        case 'SET_KEYS':
            return { ...state, keys: action.payload };
            
        case 'LOGOUT':
            return { ...state, keys: INITIAL_SESSION_STATE.keys };
        default:
            return state;
    }
};