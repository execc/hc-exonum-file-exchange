export const inboxMock = {
    items: [{
        fileName: 'happy-hacking.gif', timestamp: new Date(2018, 0, 1, 9, 0, 0), 
        uuid: '1234', from: '1234', to: '1234', type: 1234, hash: 'string' 
    }]
}