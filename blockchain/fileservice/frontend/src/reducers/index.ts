import { combineReducers } from 'redux';
import { send } from './send';
import { inbox } from './inbox';
import { exonum } from './exonum';
import { session } from './session';

export const rootReducer = combineReducers({ send, inbox, exonum, session });