const INITIAL_EXONUM_STATE = {
    newMessage: null,
    keys: {
        publicKey: null,
        secretKey: null
    }
}

export const exonum = (state = INITIAL_EXONUM_STATE, action: any) => {
    switch (action.type) {
        case 'EXONUM_SET_KEYPAIR':
            return {...state, keys: action.payload};
        case 'EXONUM_NEW_MESSAGE':
            return {...state, newMessage: action.payload};        
        default:
            return state;
    }
};