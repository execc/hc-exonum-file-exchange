rm -rf singlenode
mkdir singlenode
.\target\debug\blockchain_fileservice.exe  generate-template singlenode/common.toml --validators-count 1
.\target\debug\blockchain_fileservice.exe  generate-config singlenode/common.toml  singlenode/pub_1.toml singlenode/sec_1.toml --peer-address 127.0.0.1:6331
.\target\debug\blockchain_fileservice.exe  finalize --public-api-address 0.0.0.0:8200 --private-api-address 0.0.0.0:8091 singlenode/sec_1.toml singlenode/node_1_cfg.toml --public-configs singlenode/pub_1.toml
.\target\debug\blockchain_fileservice.exe  run --node-config singlenode/node_1_cfg.toml --db-path singlenode/db1 --public-api-address 0.0.0.0:8200
