//! This module contains basic blockchain transaction functionality for file exhcnange service
//! 

#![deny(missing_debug_implementations, missing_docs, unsafe_code, bare_trait_objects)]

extern crate chrono;
#[macro_use]
extern crate exonum;
extern crate exonum_time;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate log;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate ipfsapi;

pub mod api;
pub mod schema;
pub mod transactions;

use exonum::{
    api::ServiceApiBuilder, blockchain::{self, Transaction, TransactionSet}, crypto::Hash,
    encoding::Error as StreamStructError, helpers::fabric, messages::RawTransaction,
    storage::Snapshot,
};

use api::PublicApi;
use schema::Schema;
use transactions::FileTransactions;

// Timestamping service UID and name
//
const FILE_SERVICE: u16 = 130;
const SERVICE_NAME: &str = "fileservice";

/// Exonum `Service` implementation.
#[derive(Debug, Default)]
pub struct Service;

impl blockchain::Service for Service {
    fn service_id(&self) -> u16 {
        FILE_SERVICE
    }

    fn service_name(&self) -> &'static str {
        SERVICE_NAME
    }

    fn state_hash(&self, view: &dyn Snapshot) -> Vec<Hash> {
        let schema = Schema::new(view);
        schema.state_hash()
    }

    fn tx_from_raw(&self, raw: RawTransaction) -> Result<Box<dyn Transaction>, StreamStructError> {
        let tx = FileTransactions::tx_from_raw(raw)?;
        Ok(tx.into())
    }

    fn wire_api(&self, builder: &mut ServiceApiBuilder) {
        PublicApi::wire(builder);
    }
}

/// A configuration service creator for the `NodeBuilder`.
#[derive(Debug, Clone, Copy)]
pub struct ServiceFactory;

/// A service factory for file service
impl fabric::ServiceFactory for ServiceFactory {
    fn service_name(&self) -> &str {
        SERVICE_NAME
    }

    fn make_service(&mut self, _: &fabric::Context) -> Box<dyn blockchain::Service> {
        Box::new(Service)
    }
}
