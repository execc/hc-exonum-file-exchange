//! File service transactions.

// Workaround for `failure` see https://github.com/rust-lang-nursery/failure/issues/223 and
// ECR-1771 for the details.
#![allow(bare_trait_objects)]
// Suppress a warning in `transactions!` macro call:
#![cfg_attr(feature = "cargo-clippy", allow(redundant_field_names))]

use exonum::{
    blockchain::{ExecutionError, ExecutionResult, Transaction},
    crypto::{CryptoHash, PublicKey},
    messages::Message,
    storage::Fork,
};
use exonum_time::schema::TimeSchema;

use schema::{Schema, FileData, FileEntry};
use FILE_SERVICE;

use ipfsapi::IpfsApi;

/// Error codes emitted by wallet transactions during execution.
#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    /// Content hash already exists.
    #[fail(display = "Content hash already exists")]
    HashAlreadyExists = 0,

    /// IPFS file does not exists.
    #[fail(display = "File is not found in IPFS")]
    IpfsNotExists = 1,
}

impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = value.to_string();
        ExecutionError::with_description(value as u8, description)
    }
}

transactions! {
    /// Transaction group.
    pub FileTransactions {
        const SERVICE_ID = FILE_SERVICE;

        /// A timestamp transaction.
        struct TxFile {
            /// Public key of file sender
            sender: &PublicKey,

            /// Public key of file receiver
            receiver: &PublicKey,

            /// File content.
            content: FileData,
        }
    }
}

impl Transaction for TxFile {
    fn verify(&self) -> bool {
        // Validate transaction sender public key

        //

        // let signatureCurrect = self.verify_signature(self.sender());
        // if !signatureCurrect {
        //     return false;
        // }

        // if self.sender() == self.receiver() {
        //     return false;
        // }

        //assert_eq!(stats.hash(), IPFS_HELLO.to_string());

        return true;
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        // Get timestamp from Time Oracle service
        //
        let time = TimeSchema::new(&fork)
            .time()
            .get()
            .expect("Can't get the time");

        let content = self.content();
        let hash = content.file_hash();
        let ipfs_hash = content.ipfs_hash();

        // Validate that no file with same hash is already added
        //
        let mut schema = Schema::new(fork);
        if let Some(_entry) = schema.files().get(hash) {
            Err(Error::HashAlreadyExists)?;
        }

        // Validate that file exists in IPFS
        //
        let api = IpfsApi::new("127.0.0.1", 5001);
        let stats = api.object_stats(ipfs_hash).unwrap();

        if stats.hash() != ipfs_hash.to_string() {
            Err(Error::IpfsNotExists)?;
        }

        trace!("File added: {:?}", self);
        let entry = FileEntry::new(
            self.content(),     // File content
            &self.hash(),       // Transaction hash
            &self.sender(),     // File sender
            self.receiver(),    // File receiver
            time                // File timestamp entry
        );
        schema.add_file(entry);
        Ok(())
    }
}
