//! Timestamping database schema.

use chrono::{DateTime, Utc};
use exonum::crypto::{Hash, PublicKey};
use exonum::{
    storage::{Fork, ProofMapIndex, ListIndex, Snapshot},
};

/// Persisted structures declaration
/// 
encoding_struct! {

    /// Stores information about data in single file (content hash, metadata).
    /// 
    struct FileData {
        /// File unique identifier (hash)
        file_hash: &Hash,

        /// File exchange unique identifier (uuid)
        exchange_uuid: &Hash,

        /// IPFS Hash
        ipfs_hash: &str,

        /// File description (comment)
        description: &str,

        /// File name 
        file_name: &str,

        /// File MIME type
        file_type: &str,
    }
}

encoding_struct! {

    /// Stores information about single file entry, happended during some file exchage
    /// 
    struct FileEntry {
        /// File (meta) data
        data: FileData,

        /// Hash of transaction. Used to lookup transaction thet
        /// led to adding this file
        tx_hash: &Hash,

        /// Sender 
        sender: &PublicKey,
        
        /// Receiver
        receiver: &PublicKey,

        /// Timestamp time.
        time: DateTime<Utc>,
    }
}

/// File database schema.
/// 
#[derive(Debug)]
pub struct Schema<T> {
    view: T,
}

/// File database schema implementation
/// 
impl<T> Schema<T> {
    /// Creates a new schema from the database view.
    pub fn new(snapshot: T) -> Self {
        Schema { view: snapshot }
    }
}

/// File database schema implementation (read only/view)
/// 
impl<T> Schema<T>
where
    T: AsRef<dyn Snapshot>,
{
    /// Returns the `ProofMapIndex` of files.
    pub fn files(&self) -> ProofMapIndex<&T, Hash, FileEntry> {
        ProofMapIndex::new("fileservice.files", &self.view)
    }

    /// Helper index to search based on file sender
    pub fn sender_files(&self, sender: &PublicKey) -> ListIndex<&T, Hash> {
        ListIndex::new_in_family("fileservice.sender_files", sender, &self.view)
    }

    /// Helper index to search based on file receiver
    pub fn receiver_files(&self, receiver: &PublicKey) -> ListIndex<&T, Hash> {
        ListIndex::new_in_family("fileservice.receiver_files", receiver, &self.view)
    }

    /// Helper index to search based on exchange id
    pub fn exchange_files(&self, exchange: &Hash) -> ListIndex<&T, Hash> {
        ListIndex::new_in_family("fileservice.exchange_files", exchange, &self.view)
    }

    /// Returns the state hash of the timestamping service.
    pub fn state_hash(&self) -> Vec<Hash> {
        vec![
            self.files().merkle_root()
        ]
    }
}
/// File database schema implementation (write/table)
/// 
impl<'a> Schema<&'a mut Fork> {
    /// Returns the mutable `ProofMapIndex` of files.
    pub fn files_mut(&mut self) -> ProofMapIndex<&mut Fork, Hash, FileEntry> {
        ProofMapIndex::new("fileservice.files", &mut self.view)
    }

    /// Helper index to store association from sender to his files
    pub fn sender_files_mut(&mut self, sender: &PublicKey) -> ListIndex<&mut Fork, Hash> {
        ListIndex::new_in_family("fileservice.sender_files", sender, self.view)
    }

    /// Helper index to store association from receiver_files_mut to his files
    pub fn receiver_files_mut(&mut self, receiver: &PublicKey) -> ListIndex<&mut Fork, Hash> {
        ListIndex::new_in_family("fileservice.receiver_files", receiver, self.view)
    }

    /// Helper index to store association exchange id to his files
    pub fn exchange_files_mut(&mut self, exchange: &Hash) -> ListIndex<&mut Fork, Hash> {
        ListIndex::new_in_family("fileservice.exchange_files", exchange, self.view)
    }

    /// Persistance logic
    /// Adds the file entry to the database.
    pub fn add_file(&mut self, file_entry: FileEntry) {
        let data = file_entry.data();

        let sender = *file_entry.sender();
        let receiver = *file_entry.receiver();
        let exchange = data.exchange_uuid();

        let file_hash = data.file_hash();

        // Check that timestamp with given content_hash does not exist.
        if self.files().contains(file_hash) {
            return;
        }

        // Add timestamp to main table and indexes
        self.files_mut().put(file_hash, file_entry);
        self.sender_files_mut(&sender).push(*file_hash);
        self.receiver_files_mut(&receiver).push(*file_hash);
        self.exchange_files_mut(&exchange).push(*file_hash);
    }
}
