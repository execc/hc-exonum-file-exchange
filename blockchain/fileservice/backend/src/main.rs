extern crate exonum;
extern crate exonum_configuration;
extern crate exonum_time;

extern crate blockchain_fileservice;


use exonum::helpers::fabric::NodeBuilder;

fn main() {
    exonum::helpers::init_logger().unwrap();
    NodeBuilder::new()
        .with_service(Box::new(exonum_configuration::ServiceFactory))
        .with_service(Box::new(exonum_time::TimeServiceFactory))
        .with_service(Box::new(blockchain_fileservice::ServiceFactory))
        .run();
}
