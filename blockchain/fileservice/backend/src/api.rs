//! This module contains file service REST API
//! 

use exonum::{
    api::{self, ServiceApiBuilder, ServiceApiState}, blockchain::{self, BlockProof},
    crypto::{CryptoHash, Hash, PublicKey}, node::TransactionSend, storage::MapProof,
};

use schema::{Schema, FileEntry};
use transactions::{TxFile};
use FILE_SERVICE;

/// Describes query parameters for `handle_file`, `handle_file_proof` and `handle_exchange` endpoints.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct HashQuery {
    /// Hash of the requested file.
    pub hash: Hash,
}

impl HashQuery {
    /// Creates new `HashQuery` with given `hash`.
    pub fn new(hash: Hash) -> Self {
        HashQuery { hash }
    }
}

/// Describes query parameters for `handle_sender` and `handle_receiver` endpoints.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct KeyQuery {
    /// Address in blockchain
    pub key: PublicKey,
}

impl KeyQuery {
    /// Creates new `KeyQuery` with given `key`.
    pub fn new(key: PublicKey) -> Self {
        KeyQuery { key }
    }
}

/// Describes the information required to prove the correctness of the file entries.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FileProof {
    /// Proof of the last block.
    pub block_info: BlockProof,
    /// Actual state hashes of the timestamping service with their proofs.
    pub state_proof: MapProof<Hash, Hash>,
    /// Actual state of the timestamping database with proofs.
    pub file_proof: MapProof<Hash, FileEntry>,
}

/// Public service API.
#[derive(Debug, Clone, Copy)]
pub struct PublicApi;

impl PublicApi {
    /// Endpoint for handling timestamping transactions.
    pub fn handle_post_transaction(
        state: &ServiceApiState,
        transaction: TxFile,
    ) -> api::Result<Hash> {
        let hash = transaction.hash();
        state.sender().send(transaction.into())?;
        Ok(hash)
    }

    /// Endpoint for getting a single file.
    pub fn handle_file(
        state: &ServiceApiState,
        query: HashQuery,
    ) -> api::Result<Option<FileEntry>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(&snapshot);
        Ok(schema.files().get(&query.hash))
    }
    

    /// Endpoint for getting the proof of a single file.
    pub fn handle_file_proof(
        state: &ServiceApiState,
        query: HashQuery,
    ) -> api::Result<FileProof> {
        let snapshot = state.snapshot();
        let (state_proof, block_info) = {
            let core_schema = blockchain::Schema::new(&snapshot);
            let last_block_height = state.blockchain().last_block().height();
            let block_proof = core_schema.block_and_precommits(last_block_height).unwrap();
            let state_proof = core_schema.get_proof_to_service_table(FILE_SERVICE, 0);
            (state_proof, block_proof)
        };
        let schema = Schema::new(&snapshot);
        let file_proof = schema.files().get_proof(query.hash);
        Ok(FileProof {
            block_info,
            state_proof,
            file_proof,
        })
    }

    /// Endpoint for getting a list of files by sender key
    pub fn handle_sender(
        state: &ServiceApiState,
        query: KeyQuery,
    ) -> api::Result<Option<Vec<FileEntry>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        let sender_files = schema.sender_files(&query.key);
        let files = sender_files.into_iter().map(| hash | {
            schema.files().get(&hash)
        }).collect::<Option<Vec<FileEntry>>>();
        Ok(files)
    }

    /// Endpoint for getting a list of files by receiver key
    pub fn handle_receiver(
        state: &ServiceApiState,
        query: KeyQuery,
    ) -> api::Result<Option<Vec<FileEntry>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        let receiver_files = schema.receiver_files(&query.key);
        let files = receiver_files.into_iter().map(| hash | {
            schema.files().get(&hash)
        }).collect::<Option<Vec<FileEntry>>>();
        Ok(files)
    }

        /// Endpoint for getting a list of files by receiver key
    pub fn handle_exchange(
        state: &ServiceApiState,
        query: HashQuery,
    ) -> api::Result<Option<Vec<FileEntry>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        let exchange_files = schema.exchange_files(&query.hash);
        let files = exchange_files.into_iter().map(| hash | {
            schema.files().get(&hash)
        }).collect::<Option<Vec<FileEntry>>>();
        Ok(files)
    }

    /// Wires the above endpoints to public API scope of the given `ServiceApiBuilder`.
    pub fn wire(builder: &mut ServiceApiBuilder) {
        builder
            .public_scope()
            // Search file by hash
            .endpoint("v1/files/value", Self::handle_file)

            // Get file existence proof
            .endpoint("v1/files/proof", Self::handle_file_proof)

            // Search by sender and receiver
            .endpoint("v1/files/sender", Self::handle_sender)
            .endpoint("v1/files/receiver", Self::handle_receiver)

            // Search by exchange
            .endpoint("v1/files/exchange", Self::handle_exchange)

            // Add new file to blockchain
            .endpoint_mut("v1/files", Self::handle_post_transaction);
    }
}
