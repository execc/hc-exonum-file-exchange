#[macro_use]
extern crate serde_json;

#[macro_use]
extern crate exonum_testkit;

extern crate blockchain_fileservice;
extern crate exonum;
extern crate exonum_time;

extern crate ipfsapi;

use exonum::{
    api::{self},
    api::node::public::explorer::TransactionQuery,
    blockchain::Transaction,
    crypto::{gen_keypair, hash, CryptoHash, Hash},
    helpers::Height,
};
use exonum_testkit::{ApiKind, TestKit, TestKitApi, TestKitBuilder};
use exonum_time::{time_provider::MockTimeProvider, TimeService};

use std::time::SystemTime;

use blockchain_fileservice::{
    api::{HashQuery, KeyQuery},
    schema::{FileData, FileEntry},
    transactions::{TxFile},
    Service,
};

use ipfsapi::IpfsApi;

const IPFS_HELLO: &str = "QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u";

fn init_testkit() -> (TestKit, MockTimeProvider) {
    let mock_provider = MockTimeProvider::new(SystemTime::now().into());
    let mut testkit = TestKitBuilder::validator()
        .with_service(Service)
        .with_service(TimeService::with_provider(mock_provider.clone()))
        .create();
    testkit.create_blocks_until(Height(2)); // TimeService is None if no blocks were forged
    (testkit, mock_provider)
}

/// Assert transaction status
fn assert_status(api: &TestKitApi, tx: &Transaction, expected_status: &serde_json::Value) {
    let info: serde_json::Value = api
        .public(ApiKind::Explorer)
        .query(&TransactionQuery::new(tx.hash()))
        .get("v1/transactions")
        .unwrap();

    if let serde_json::Value::Object(mut info) = info {
        let tx_status = info.remove("status").unwrap();
        assert_eq!(tx_status, *expected_status);
    } else {
        panic!("Invalid transaction info format, object expected");
    }
}

#[test]
fn test_api_get_file_nothing() {
    let (testkit, _) = init_testkit();
    let api = testkit.api();
    let entry: Option<FileEntry> = api
        .public(ApiKind::Service("fileservice"))
        .query(&HashQuery::new(Hash::zero()))
        .get("v1/files/value")
        .unwrap();

    assert!(entry.is_none());
}

#[test]
fn test_api_post_file() {
    let (testkit, _) = init_testkit();

    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();
    let tx = TxFile::new(&keypair1.0, &keypair2.0, info, &keypair1.1);

    let api = testkit.api();
    let tx_hash: Hash = api
        .public(ApiKind::Service("fileservice"))
        .query(&tx)
        .post("v1/files")
        .unwrap();

    assert_eq!(tx.hash(), tx_hash);
}

#[test]
fn test_api_post_file_invalid_same_keys() {
    let (testkit, _) = init_testkit();

    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let keypair1 = gen_keypair();
    let tx = TxFile::new(&keypair1.0, &keypair1.0, info, &keypair1.1);

    let api = testkit.api();
    let tx_hash: api::Result<Hash> = api
        .public(ApiKind::Service("fileservice"))
        .query(&tx)
        .post("v1/files");

    assert_eq!(true, tx_hash.is_err());
}

#[test]
fn test_api_post_file_invalid_ipfs() {
    let (testkit, _) = init_testkit();

    let info = FileData::new(&Hash::zero(), &Hash::zero(), "123", "Example PDF file", "example.pdf", "application/pdf");
    let keypair1 = gen_keypair();
    let tx = TxFile::new(&keypair1.0, &keypair1.0, info, &keypair1.1);

    let api = testkit.api();
    let tx_hash: api::Result<Hash> = api
        .public(ApiKind::Service("fileservice"))
        .query(&tx)
        .post("v1/files");

    assert_eq!(true, tx_hash.is_err());
}

#[test]
fn test_api_get_file_proof() {
    let (mut testkit, _) = init_testkit();

    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();

    // Create file
    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let tx = TxFile::new(&keypair1.0, &keypair2.0, info, &keypair1.1);
    testkit.create_block_with_transactions(txvec![tx]);

    // get proof
    let api = testkit.api();
    let _: serde_json::Value = api
        .public(ApiKind::Service("fileservice"))
        .query(&HashQuery::new(Hash::zero()))
        .get("v1/files/proof")
        .unwrap();
}

#[test]
fn test_api_get_file_entry() {
    let (mut testkit, _) = init_testkit();

    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();

    // Create file
    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let tx = TxFile::new(&keypair1.0, &keypair2.0, info.clone(), &keypair1.1);
    testkit.create_block_with_transactions(txvec![tx.clone()]);

    let api = testkit.api();
    let entry: Option<FileEntry> = api
        .public(ApiKind::Service("fileservice"))
        .query(&HashQuery::new(Hash::zero()))
        .get("v1/files/value")
        .unwrap();

    let entry = entry.unwrap();
    assert_eq!(entry.data(), info);
    assert_eq!(entry.tx_hash(), &tx.hash());
}

#[test]
fn test_api_get_file_entry_by_sender() {
    let (mut testkit, _) = init_testkit();

    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();

    // Create file
    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let tx = TxFile::new(&keypair1.0, &keypair2.0, info.clone(), &keypair1.1);
    testkit.create_block_with_transactions(txvec![tx.clone()]);

    let api = testkit.api();
    let entry: Option<Vec<FileEntry>> = api
        .public(ApiKind::Service("fileservice"))
        .query(&KeyQuery::new(keypair1.0))
        .get("v1/files/sender")
        .unwrap();

    let entry = entry.unwrap();
    let entry = entry.first().unwrap();

    assert_eq!(entry.data(), info);
    assert_eq!(entry.tx_hash(), &tx.hash());
}

#[test]
fn test_api_get_file_entry_by_receiver() {
    let (mut testkit, _) = init_testkit();

    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();

    // Create file
    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let tx = TxFile::new(&keypair1.0, &keypair2.0, info.clone(), &keypair1.1);
    testkit.create_block_with_transactions(txvec![tx.clone()]);

    let api = testkit.api();
    let entry: Option<Vec<FileEntry>> = api
        .public(ApiKind::Service("fileservice"))
        .query(&KeyQuery::new(keypair2.0))
        .get("v1/files/receiver")
        .unwrap();

    let entry = entry.unwrap();
    let entry = entry.first().unwrap();

    assert_eq!(entry.data(), info);
    assert_eq!(entry.tx_hash(), &tx.hash());
}

#[test]
fn test_api_get_file_entries_by_exchange() {
    let (mut testkit, _) = init_testkit();

    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();

    // Create file
    let info = FileData::new(&Hash::zero(), &Hash::zero(), IPFS_HELLO, "Example PDF file", "example.pdf", "application/pdf");
    let tx = TxFile::new(&keypair1.0, &keypair2.0, info.clone(), &keypair1.1);
    testkit.create_block_with_transactions(txvec![tx.clone()]);

    let api = testkit.api();
    let entry: Option<Vec<FileEntry>> = api
        .public(ApiKind::Service("fileservice"))
        .query(&HashQuery::new(Hash::zero()))
        .get("v1/files/exchange")
        .unwrap();

    let entry = entry.unwrap();
    let entry = entry.first().unwrap();

    assert_eq!(entry.data(), info);
    assert_eq!(entry.tx_hash(), &tx.hash());
}

#[test]
fn test_api_can_not_add_same_content_hash() {
    let (mut testkit, _) = init_testkit();
    let api = testkit.api();

    let keypair1 = gen_keypair();
    let keypair2 = gen_keypair();
    let content_hash = hash(&[1]);
    let file1 = FileData::new(&content_hash, &Hash::zero(), IPFS_HELLO, "metadata", "example.pdf", "application/pdf");
    let file2 = FileData::new(&content_hash, &Hash::zero(), IPFS_HELLO, "other metadata", "example.pdf", "application/pdf");
    let tx_ok =  TxFile::new(&keypair1.0, &keypair2.0, file1.clone(), &keypair1.1);
    let tx_err = TxFile::new(&keypair1.0, &keypair2.0, file2.clone(), &keypair1.1);

    testkit.create_block_with_transaction(tx_ok.clone());
    assert_status(&api, &tx_ok, &json!({ "type": "success" }));

    testkit.create_block_with_transaction(tx_err.clone());
    assert_status(
        &api,
        &tx_err,
        &json!({ "type": "error", "code": 0, "description": "Content hash already exists" }),
    );
}

#[test]
fn test_ipfs_test_object_stats() {
    let api = IpfsApi::new("127.0.0.1", 5001);
    // Hello world object
    let stats = api.object_stats(IPFS_HELLO).unwrap();
    
    assert_eq!(stats.hash(), IPFS_HELLO.to_string());
    assert_eq!(stats.cumulative_size(), 20);
}